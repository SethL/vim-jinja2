function! s:JinjaType()
	" autodetect filetype
	execute 'doautocmd filetypedetect BufRead ' . fnameescape(expand('<afile>:r'))
	if &ft == ''	| execute 'setlocal ft=jinja2'
	else		| execute 'setlocal ft=' . &ft . '.jinja2'
	endif

	" command to manually set type
	command! -nargs=1 JinjaFile set filetype=<args>.jinja2
	nnoremap <buffer><special> <localleader>j :JinjaFile<space>
endfunction

augroup DetectJinjaFileType
	autocmd!
	autocmd BufNewFile,BufRead *.j2		call s:JinjaType()
augroup END
